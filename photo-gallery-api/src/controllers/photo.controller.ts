import {Request, Response} from 'express'
import fs from 'fs-extra';
import path from 'path'

//Models
import Photo from '../models/Photo';

export async function createPhoto(req: Request, res: Response): Promise<Response> {
    const { name, appearance, power, description } = req.body;
    console.log(req.file);
    const newPhoto = {
        name: name,
        appearance: appearance,
        power: power,
        description: description,
        imagePath: req.file.path
    };
    const photo = new Photo(newPhoto);
    await photo.save();
    return res.json({
        message: 'Héroe guardado con éxito',
        photo
    })
}

export async function getPhotos(req: Request, res: Response): Promise<Response> {
    const photos = await Photo.find();
    return res.json(photos);
}

export async function getPhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const photo = await Photo.findById(id);
    return res.json(photo);
}

export async function deletePhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const photo = await Photo.findByIdAndRemove(id);
    if (photo) {
        await fs.unlink(path.resolve(photo.imagePath));
    }
    return res.json({ 
        message: 'Photo Deleted', 
        photo 
    });
};

export async function updatePhoto(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { name, appearance, power, description } = req.body;
    console.log(req.body);
    const updatedPhoto = await Photo.findByIdAndUpdate(id, {
        name: name,
        appearance: appearance,
        power: power,
        description: description
    }, {new: true});
    return res.json({
        message: 'Actualizado exitosamente',
        updatedPhoto
    });
}