import {Schema, model, Document} from 'mongoose';

const schema = new Schema({
    name: String,
    imagePath: String,
    appearance: String,
    power: String,
    description: String
});

interface IPhoto extends Document {
    name: string,
    imagePath: string,
    appearance: string,
    power: string,
    description: string
}

export default model<IPhoto>('Photo', schema);