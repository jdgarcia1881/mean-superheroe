export interface Photo {
    _id?: string;
    name: string,
    appearance: string,
    power: string,
    description: string,
    imagePath: string
}