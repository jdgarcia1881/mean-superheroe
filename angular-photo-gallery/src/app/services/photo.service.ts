import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

import {Photo} from '../interfaces/Photo'

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  URI = 'http://localhost:4003/api/photos'

  constructor(private http: HttpClient) { }

  createPhoto(name: string, appearance: string, power: string, description: string, photo: File) {
    const fd = new FormData();
    fd.append('name', name);
    fd.append('appearance', appearance);
    fd.append('power', power);
    fd.append('description', description);
    fd.append('image', photo);
    return this.http.post(this.URI, fd)
  }

  getPhotos() {
    return this.http.get<Photo[]>(this.URI);
  }

  getPhoto(id: string) {
    return this.http.get<Photo>(`${this.URI}/${id}`);
  }

  deletePhoto(id: string) {
    return this.http.delete(`${this.URI}/${id}`);
  }

  updatePhoto(id: string, name: string, appearance: string,  power: string, description: string) {
    return this.http.put(`${this.URI}/${id}`, {name, appearance, power, description});
  }
}
